#-          Timestamp : date and time URL was found (format: mm/dd/yyyy hh:mm)
#-          Linking URL : URL which leads to Hosting URL
#-          Hosting URL : URL which leads to hosted content in the form of Direct stream or direct download should be available through this link.
#-          Title : Title name from section 2.

import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen 
import lxml
import pandas as pd

stream_name = input("Please enter a stream name:\n")
format_stream_name = stream_name.replace(' ', '+')
print(r'Searching {stream_name}. Wait please...')

#Fetching all items
data = {
    'Title': [],
    'Linking URL': []
}

base_url = 'https://filmstoon.in/'
search_url = f'https://filmstoon.in/?s={format_stream_name}'



def export_table_and_print(data):
    table = pd.DataFrame(data, columns=['Title', 'Linking URL'])
    table.to_csv(f'{stream_name}_evidence.csv', sep='|', encoding="utf-8", index=False)
    print(table)

def get_stream_attributes(stream):
    #Getting the stream attributes
    title = stream.find('img', class_ = "mli-thumb")['alt']
    url = stream.find('a', class_ = "ml-mask jt")['href']


    #Store the values into the 'data' object
    data['Title'].append(title)
    data['Linking URL'].append(url)



#HTTP Get request
page = requests.get(search_url)

#Checking if we successfully fetched the URL
if page.status_code == requests.codes.ok:
    bs = BeautifulSoup(page.text, 'lxml')   



    list_all_streams = bs.findAll('div', class_ = 'ml-item')
    for stream in list_all_streams:
        get_stream_attributes(stream)

    export_table_and_print(data)